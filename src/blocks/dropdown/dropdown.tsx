import * as React from "react";
import * as Tripetto from "tripetto-collector";
import { Field } from "redux-form";
import { NodeBlock } from "../../block";
import { IDropdown, IDropdownOption } from "tripetto-block-dropdown";
import "./condition";

interface IDropdownValues {
    option: string;
}

@Tripetto.node("tripetto-block-dropdown")
export class Dropdown extends NodeBlock<IDropdownValues, IDropdown> {
    public Values(instance: Tripetto.Instance, values?: IDropdownValues): IDropdownValues {
        const selected = this.DataAssert<string>(instance, "option");

        if (values) {
            const value = Tripetto.F.FindFirst(this.Props.Options, (option: IDropdownOption) => option.Id === values.option);

            selected.Set(value ? value.Value || value.Name : undefined, values.option);
        }

        return {
            option: selected.Reference
        };
    }

    public Fields(instance: Tripetto.Instance): () => JSX.Element {
        const required = this.SlotAssert("option").Required;

        return () => {
            return (
                <div className="form-group">
                    {this.Meta.Name &&
                        this.Meta.NameVisible && (
                            <label>
                                {this.Meta.Name}
                                {required && <span className="text-danger">*</span>}
                            </label>
                        )}
                    {this.Meta.Description && <p>{this.Meta.Description}</p>}
                    <Field name="option" component="select" className="form-control">
                        {this.Meta.Placeholder && <option value="">{this.Meta.Placeholder}</option>}
                        {this.Props.Options.map((option: IDropdownOption) => (
                            <option key={option.Id} value={option.Id}>
                                {option.Name}
                            </option>
                        ))}
                    </Field>
                    {this.Meta.Explanation && <span className="help-block">{this.Meta.Explanation}</span>}
                </div>
            );
        };
    }

    public OnValidate(instance: Tripetto.Instance): boolean {
        const slot = this.SlotAssert("option");
        const dropdown = this.DataAssert<string>(instance, slot);

        return !slot.Required || dropdown.Reference !== "";
    }
}
