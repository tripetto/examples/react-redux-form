import * as React from "react";
import * as Tripetto from "tripetto-collector";
import { Field } from "redux-form";
import { NodeBlock } from "../../block";
import { IEmail } from "tripetto-block-email";

/* tslint:disable-next-line:max-line-length */
const IS_EMAIL = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

interface IEmailValues {
    email: string;
}

@Tripetto.node("tripetto-block-email")
export class Email extends NodeBlock<IEmailValues, IEmail> {
    protected Values(instance: Tripetto.Instance, values?: IEmailValues): IEmailValues {
        const data = this.DataAssert<string>(instance, "email");

        if (values) {
            data.Value = values.email;
        }

        return {
            email: data.Value
        };
    }

    public Fields(instance: Tripetto.Instance): () => JSX.Element {
        const required = this.SlotAssert("email").Required;

        return () => (
            <div className="form-group">
                {this.Meta.Name &&
                    this.Meta.NameVisible && (
                        <label>
                            {this.Meta.Name}
                            {required && <span className="text-danger">*</span>}
                        </label>
                    )}
                {this.Meta.Description && <p>{this.Meta.Description}</p>}
                <div className="input-group">
                    <span className="input-group-addon">@</span>
                    <Field name="email" component="input" type="email" placeholder={this.Meta.Placeholder} className="form-control" />
                </div>
                {this.Meta.Explanation && <span className="help-block">{this.Meta.Explanation}</span>}
            </div>
        );
    }

    public OnValidate(instance: Tripetto.Instance): boolean {
        const slot = this.SlotAssert("email");
        const email = this.DataAssert<string>(instance, slot);

        if (slot.Required && email.Value === "") {
            return false;
        }

        if (email.Value !== "" && !IS_EMAIL.test(email.Value)) {
            return false;
        }

        return true;
    }
}
