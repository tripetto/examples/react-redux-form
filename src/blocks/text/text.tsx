import * as React from "react";
import * as Tripetto from "tripetto-collector";
import { Field } from "redux-form";
import { NodeBlock } from "../../block";
import { IText } from "tripetto-block-text";
import "./condition";

interface ITextValues {
    value: string;
}

@Tripetto.node("tripetto-block-text")
export class Text extends NodeBlock<ITextValues, IText> {
    protected Values(instance: Tripetto.Instance, values?: ITextValues): ITextValues {
        const data = this.DataAssert<string>(instance, "value");

        if (values) {
            data.Value = values.value;
        }

        return {
            value: data.Value
        };
    }

    protected Fields(instance: Tripetto.Instance): () => JSX.Element {
        const required = this.SlotAssert("value").Required;

        return () => {
            return (
                <div className="form-group">
                    {this.Meta.Name &&
                        this.Meta.NameVisible && (
                            <label>
                                {this.Meta.Name}
                                {required && <span className="text-danger">*</span>}
                            </label>
                        )}
                    {this.Meta.Description && <p>{this.Meta.Description}</p>}
                    <Field name="value" component="input" type="text" placeholder={this.Meta.Placeholder} className="form-control" />
                    {this.Meta.Explanation && <span className="help-block">{this.Meta.Explanation}</span>}
                </div>
            );
        };
    }

    public OnValidate(instance: Tripetto.Instance): boolean {
        const slot = this.SlotAssert("value");
        const value = this.DataAssert<string>(instance, slot);

        return !slot.Required || value.Value !== "";
    }
}
