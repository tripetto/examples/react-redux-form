import * as React from "react";
import * as Tripetto from "tripetto-collector";
import { Field } from "redux-form";
import { NodeBlock } from "../../block";
import { IURL } from "tripetto-block-url";

/* tslint:disable-next-line:max-line-length */
const IS_URL = /^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$/i;

interface IURLValues {
    url: string;
}

@Tripetto.node("tripetto-block-url")
export class URL extends NodeBlock<IURLValues, IURL> {
    protected Values(instance: Tripetto.Instance, values?: IURLValues): IURLValues {
        const data = this.DataAssert<string>(instance, "url");

        if (values) {
            data.Value = values.url;
        }

        return {
            url: data.Value
        };
    }

    public Fields(instance: Tripetto.Instance): () => JSX.Element {
        const required = this.SlotAssert("url").Required;

        return () => (
            <div className="form-group">
                {this.Meta.Name &&
                    this.Meta.NameVisible && (
                        <label>
                            {this.Meta.Name}
                            {required && <span className="text-danger">*</span>}
                        </label>
                    )}
                {this.Meta.Description && <p>{this.Meta.Description}</p>}
                <Field name="url" component="input" type="url" placeholder={this.Meta.Placeholder} className="form-control" />
                {this.Meta.Explanation && <span className="help-block">{this.Meta.Explanation}</span>}
            </div>
        );
    }

    public OnValidate(instance: Tripetto.Instance): boolean {
        const slot = this.SlotAssert("url");
        const url = this.DataAssert<string>(instance, slot);

        if (slot.Required && url.Value === "") {
            return false;
        }

        if (url.Value !== "" && !IS_URL.test(url.Value)) {
            return false;
        }

        return true;
    }
}
