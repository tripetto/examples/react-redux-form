import * as React from "react";
import * as Tripetto from "tripetto-collector";
import { Field } from "redux-form";
import { NodeBlock } from "../../block";
import { IPassword } from "tripetto-block-password";

interface IPasswordValues {
    password: string;
}

@Tripetto.node("tripetto-block-password")
export class Password extends NodeBlock<IPasswordValues, IPassword> {
    protected Values(instance: Tripetto.Instance, values?: IPasswordValues): IPasswordValues {
        const data = this.DataAssert<string>(instance, "password");

        if (values) {
            data.Value = values.password;
        }

        return {
            password: data.Value
        };
    }

    public Fields(instance: Tripetto.Instance): () => JSX.Element {
        const required = this.SlotAssert("password").Required;

        return () => (
            <div className="form-group">
                {this.Meta.Name &&
                    this.Meta.NameVisible && (
                        <label>
                            {this.Meta.Name}
                            {required && <span className="text-danger">*</span>}
                        </label>
                    )}
                {this.Meta.Description && <p>{this.Meta.Description}</p>}
                <Field name="password" component="input" type="password" placeholder={this.Meta.Placeholder} className="form-control" />
                {this.Meta.Explanation && <span className="help-block">{this.Meta.Explanation}</span>}
            </div>
        );
    }

    public OnValidate(instance: Tripetto.Instance): boolean {
        const slot = this.SlotAssert("password");
        const password = this.DataAssert<string>(instance, slot);

        return !slot.Required || password.Value !== "";
    }
}
