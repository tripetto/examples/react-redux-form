import * as React from "react";
import * as Tripetto from "tripetto-collector";
import { Field } from "redux-form";
import { NodeBlock } from "../../block";
import { INumber } from "tripetto-block-number";

interface INumberValues {
    number: number;
}

@Tripetto.node("tripetto-block-number")
export class Number extends NodeBlock<INumberValues, INumber> {
    protected Values(instance: Tripetto.Instance, values?: INumberValues): INumberValues {
        const data = this.DataAssert<number>(instance, "number");

        if (values) {
            data.Value = values.number;
        }

        return {
            number: data.Value
        };
    }

    public Fields(instance: Tripetto.Instance): () => JSX.Element {
        const required = this.SlotAssert("number").Required;

        return () => (
            <div className="form-group">
                {this.Meta.Name &&
                    this.Meta.NameVisible && (
                        <label>
                            {this.Meta.Name}
                            {required && <span className="text-danger">*</span>}
                        </label>
                    )}
                {this.Meta.Description && <p>{this.Meta.Description}</p>}
                <div className="input-group">
                    <span className="input-group-addon">#</span>
                    <Field name="number" component="input" type="number" placeholder={this.Meta.Placeholder} className="form-control" />
                </div>
                {this.Meta.Explanation && <span className="help-block">{this.Meta.Explanation}</span>}
            </div>
        );
    }

    public OnValidate(instance: Tripetto.Instance): boolean {
        const slot = this.SlotAssert("number");
        const value = this.DataAssert<number>(instance, slot);

        return !slot.Required || value.String !== "";
    }
}
