import * as React from "react";
import * as Tripetto from "tripetto-collector";
import { Field } from "redux-form";
import { NodeBlock } from "../../block";
import { IRadiobutton, IRadiobuttons } from "tripetto-block-radiobuttons";

interface IRadiobuttonValues {
    button: string;
}

@Tripetto.node("tripetto-block-radiobuttons")
export class Radiobuttons extends NodeBlock<IRadiobuttonValues, IRadiobuttons> {
    public Values(instance: Tripetto.Instance, values?: IRadiobuttonValues): IRadiobuttonValues {
        const selected = this.DataAssert<string>(instance, "button");

        if (values) {
            const button = Tripetto.F.FindFirst(this.Props.Radiobuttons, (radiobutton: IRadiobutton) => radiobutton.Id === values.button);

            selected.Set(button ? button.Value || button.Name : undefined, button ? button.Id : undefined);
        }

        if (!selected.Value) {
            const button = Tripetto.F.ArrayItem(this.Props.Radiobuttons, 0);

            selected.Set(button ? button.Value || button.Name : undefined, button ? button.Id : undefined);
        }

        return {
            button: selected.Reference
        };
    }

    public Fields(instance: Tripetto.Instance): () => JSX.Element {
        return () => {
            return (
                <div className="form-group">
                    {this.Meta.Name && this.Meta.NameVisible && <label>{this.Meta.Name}</label>}
                    {this.Meta.Description && <p>{this.Meta.Description}</p>}
                    {this.Props.Radiobuttons.map((radiobutton: IRadiobutton) => {
                        return (
                            <div className="radio" key={radiobutton.Id}>
                                <label>
                                    <Field name="button" component="input" type="radio" value={radiobutton.Id} />
                                    {radiobutton.Name}
                                </label>
                            </div>
                        );
                    })}
                    {this.Meta.Explanation && <span className="help-block">{this.Meta.Explanation}</span>}
                </div>
            );
        };
    }

    public OnValidate(instance: Tripetto.Instance): boolean {
        return true;
    }
}
