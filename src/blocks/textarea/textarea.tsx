import * as React from "react";
import * as Tripetto from "tripetto-collector";
import { Field } from "redux-form";
import { NodeBlock } from "../../block";
import { ITextarea } from "tripetto-block-textarea";

interface ITextareaValues {
    value: string;
}

@Tripetto.node("tripetto-block-textarea")
export class Textarea extends NodeBlock<ITextareaValues, ITextarea> {
    protected Values(instance: Tripetto.Instance, values?: ITextareaValues): ITextareaValues {
        const data = this.DataAssert<string>(instance, "value");

        if (values) {
            data.Value = values.value;
        }

        return {
            value: data.Value
        };
    }

    public Fields(instance: Tripetto.Instance): () => JSX.Element {
        const required = this.SlotAssert("value").Required;

        return () => (
            <div className="form-group">
                {this.Meta.Name &&
                    this.Meta.NameVisible && (
                        <label>
                            {this.Meta.Name}
                            {required && <span className="text-danger">*</span>}
                        </label>
                    )}
                {this.Meta.Description && <p>{this.Meta.Description}</p>}
                <Field name="value" component="textarea" rows={3} placeholder={this.Meta.Placeholder} className="form-control" />
                {this.Meta.Explanation && <span className="help-block">{this.Meta.Explanation}</span>}
            </div>
        );
    }

    public OnValidate(instance: Tripetto.Instance): boolean {
        const slot = this.SlotAssert("value");
        const value = this.DataAssert<string>(instance, slot);

        return !slot.Required || value.Value !== "";
    }
}
