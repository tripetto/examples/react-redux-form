import * as React from "react";
import * as Tripetto from "tripetto-collector";
import { Field } from "redux-form";
import { NodeBlock } from "../../block";
import { ICheckbox, ICheckboxes } from "tripetto-block-checkboxes";

interface ICheckboxesValues {
    [sId: string]: boolean;
}

@Tripetto.node("tripetto-block-checkboxes")
export class Checkboxes extends NodeBlock<ICheckboxesValues, ICheckboxes> {
    protected Values(instance: Tripetto.Instance, values?: ICheckboxesValues): ICheckboxesValues {
        const r: ICheckboxesValues = {};

        this.Props.Checkboxes.forEach((checkbox: ICheckbox) => {
            const data = this.Data<boolean>(instance, checkbox.Id);

            if (data && values) {
                data.Value = values[checkbox.Id];
            }

            r[checkbox.Id] = data ? data.Value : false;
        });

        return r;
    }

    public Fields(instance: Tripetto.Instance): () => JSX.Element {
        return () => (
            <div className="form-group">
                {this.Meta.Name && this.Meta.NameVisible && <label>{this.Meta.Name}</label>}
                {this.Meta.Description && <p>{this.Meta.Description}</p>}
                {this.Props.Checkboxes.map((checkbox: ICheckbox) => {
                    return (
                        <div key={checkbox.Id} className="checkbox">
                            <label>
                                <Field name={checkbox.Id} component="input" type="checkbox" />
                                {checkbox.Name}
                            </label>
                        </div>
                    );
                })}
                {this.Meta.Explanation && <span className="help-block">{this.Meta.Explanation}</span>}
            </div>
        );
    }

    public OnValidate(instance: Tripetto.Instance): boolean {
        return true;
    }
}
