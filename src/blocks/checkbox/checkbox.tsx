import * as React from "react";
import * as Tripetto from "tripetto-collector";
import { Field } from "redux-form";
import { NodeBlock } from "../../block";
import { ICheckbox } from "tripetto-block-checkbox";
import "./conditions";

interface ICheckboxValues {
    checked: boolean;
}

@Tripetto.node("tripetto-block-checkbox")
export class Checkbox extends NodeBlock<ICheckboxValues, ICheckbox> {
    protected Values(instance: Tripetto.Instance, values?: ICheckboxValues): ICheckboxValues {
        const data = this.DataAssert<boolean>(instance, "checked");

        if (values) {
            data.Value = values.checked;
        }

        return {
            checked: data.Value
        };
    }

    public Fields(instance: Tripetto.Instance): () => JSX.Element {
        const required = this.SlotAssert("checked").Required;

        return () => (
            <div className="form-group checkbox">
                <label>
                    <Field name="checked" component="input" type="checkbox" />
                    {this.Meta.Name}
                    {required && <span className="text-danger">*</span>}
                </label>
                {this.Meta.Explanation && <span className="help-block">{this.Meta.Explanation}</span>}
            </div>
        );
    }

    public OnValidate(instance: Tripetto.Instance): boolean {
        const slot = this.SlotAssert("checked");
        const checkbox = this.DataAssert<boolean>(instance, slot);

        return !slot.Required || checkbox.Value;
    }
}
