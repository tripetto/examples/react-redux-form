import * as React from "react";
import * as Tripetto from "tripetto-collector";
import { Provider } from "react-redux";
import { Store, combineReducers, createStore } from "redux";
import { DecoratedComponentClass, InjectedFormProps, reducer, reduxForm } from "redux-form";

export abstract class NodeBlock<Values extends {}, T extends Tripetto.INodeBlock = Tripetto.INodeBlock> extends Tripetto.NodeBlock<
    JSX.Element,
    T
> {
    /** Store for this block (we need a store per block per instance). */
    private store: {
        [instance: string]: Store<{}> | undefined;
    } = {};

    /** Store for the component. */
    private component: {
        [instance: string]: DecoratedComponentClass<Values, Partial<InjectedFormProps>> | undefined;
    } = {};

    /** Render the fields for your block. */
    protected abstract Fields(instance: Tripetto.Instance): (props: InjectedFormProps) => JSX.Element;

    /** Set or retrieve values. */
    protected abstract Values(instance: Tripetto.Instance, values?: Values): Values;

    /** Render the block. */
    public OnRender(instance: Tripetto.Instance, action: Tripetto.Await): JSX.Element {
        const Component =
            this.component[instance.Hash] ||
            (this.component[instance.Hash] = reduxForm<Values, Partial<InjectedFormProps>>({
                form: this.Meta.Id,
                onChange: (values: Partial<Values>) => this.Values(instance, values as Values),
                initialValues: this.Values(instance)
            })(this.Fields(instance)));

        // The actual state is in the Tripetto collector. We use a separate store inside each block instance for Redux Form.
        if (!this.store[instance.Hash]) {
            this.store[instance.Hash] = createStore(
                combineReducers({
                    form: reducer
                })
            );
        }

        return (
            <Provider store={this.store[instance.Hash]}>
                <Component />
            </Provider>
        );
    }

    /** Remove the store and component when the block is destroyed. */
    public OnDestroy(instance: Tripetto.Instance): void {
        this.store[instance.Hash] = undefined;
        this.component[instance.Hash] = undefined;

        super.OnDestroy(instance);
    }
}
